initfile = configuration.it.org

publish = publish.el
manifest = manifest.scm

src-folder = public/
dest-host = meup
dest-folder = /srv/sites/

darkhttps-flags = --daemon --maxconn 1 --no-listing --addr 127.0.0.1

%.el %.scm: ${initfile}
	@echo "* Tangling ${initfile}"
	@guix shell --container emacs-no-x -- emacs --batch -l org --eval '(org-babel-tangle-file "$(<)")'

bootstrap: $(publish) $(manifest)

publish: bootstrap
	@echo "* Exporting content to local web roots folder ${src-folder}"
	@guix shell --container -m ${manifest} -- emacs --batch -l ${publish} --funcall org-publish-all

deploy:
	@echo "* Syncing local web roots folder ${src-folder} to ${dest-host} web server folder ${dest-folder}"
	@guix shell rsync openssh-sans-x -- rsync -az --checksum --delete --progress -e 'ssh -F secrets/config' ${src-folder} ${dest-host}:${dest-folder}

serve-start:
	@echo "* Starting doc.meup local web server"
	@guix shell darkhttpd -- darkhttpd ${src-folder}/doc.meup/ --port 8081 --pidfile /tmp/swws-doc.meup.pid ${darkhttps-flags}
	@echo ""
	@echo "* Starting beta.swws local web server"
	@guix shell darkhttpd -- darkhttpd ${src-folder}/beta.swws/ --port 8082 --pidfile /tmp/swws-beta.swws.pid ${darkhttps-flags}
	@echo ""
	@echo "* Starting beta.meup local web server"
	@guix shell darkhttpd -- darkhttpd ${src-folder}/beta.meup/ --port 8083 --pidfile /tmp/swws-beta.meup.pid ${darkhttps-flags}
	@echo ""
	@echo "* Starting testevent.meup local web server"
	@guix shell darkhttpd -- darkhttpd ${src-folder}/testevent.meup/ --port 8084 --pidfile /tmp/swws-testevent.meup.pid ${darkhttps-flags}
	@echo ""

serve-stop:
	@echo "* Stopping doc.meup local web server"
	@kill `cat /tmp/swws-doc.meup.pid`
	@echo "* Stopping beta.swws local web server"
	@kill `cat /tmp/swws-beta.swws.pid`
	@echo "* Stopping beta.meup local web server"
	@kill `cat /tmp/swws-beta.meup.pid`
	@echo "* Stopping testevent.meup local web server"
	@kill `cat /tmp/swws-testevent.meup.pid`

clean:
	@echo "* Cleaning all output files and org-timestamp caches"
	-rm -Rf ${publish} ${manifest}
	-rm -Rf ${src-folder}*
	-rm -Rf ~/.org-timestamps/*.cache
