---
title: Realizzare video tutorial
description: Guida alla realizzazione dei video tutorial
langs: it-it
robots: noindex, nofollow
tags: Narratron, PCTO, Liceo Novello, video tutorial

---

# Realizzare video tutorial

:::info
Questo documento è impostato solo come struttura, con qualche appunto su OBS Studio scritto in fretta per rispondere alla scadenza del *rientro* a scuola di sabato 26 settembre.
Verrà progressivamente esteso nei prossimi giorni.
:::

## Il contesto

## I requisiti

## Gli strumenti

### OBS Studio

Citazione dal sito di [Indire](http://www.indire.it/tutorial-per-la-didattica-a-distanza/):
> Diciamolo subito: Open Broadcast Software (OBS) NON è semplice da usare. Lo inseriamo perchè è uno dei pochissimi software di livello  *professionale* completamente gratuiti, e perchè permette di fare veramente un pò di tutto (ad esempio la diretta streaming di una videolezione). Consigliato solo a chi... non si spaventa.

A dispetto della fama di strumento *difficile* ho trovato in Rete anche recensioni estremamente positive proprio riguardo alla facilità d'uso. Probabilmente la discriminante è il metodo con cui ci si approccia: seguire un corso in modo strutturato sembra indispensabile.

Eccone alcuni:

* Antonio Liccardo (segnalato da Indire)
http://www.indire.it/tutorial-per-la-didattica-a-distanza/

* Ivo Grimaldi - Progetto 42
https://www.youtube.com/watch?v=2uYMItMq9ig&list=PL-8hpaXWo9guNVWp26k4AbUl8dwejW86P&index=19

* Ivo Grimaldi - Playlist su OBS Studio
https://www.youtube.com/playlist?list=PLGgp-zF9PARd_vZMMinWoBo2Sm0VcOmxQ

* Andrea Ciraolo (questo ha uno stile da cui mi dissocio ...)
https://www.youtube.com/c/AndreaCiraolo/search?query=obs+studio





## Le buone pratiche

## Riferimenti per approfondire