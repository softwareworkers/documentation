---
title: Istruzioni per la marcatura semantica in CodiMD
description: Funzioni di Propp applicate alle novelle del Decameron
langs: it-it
robots: noindex, nofollow
tags: Narratron, PCTO, Liceo Novello, marcatura semantica, istruzioni

---

# Istruzioni per la marcatura semantica in CodiMD

## Struttura narrativa

Usare `Alert Area` per le quattro fasi riprese da Propp:
:::success
situazione iniziale
:::

:::info
movente
:::

:::warning
svolgimento della vicenda
:::

:::danger
conclusione
:::





### Strutture a due livelli

Usata in questo modo l'`Alert Area` indicherà che si sta applicando al testo la dimensione di analisi "struttura narrativa", e il colore (associato al tipo di **alert**: *success*, *info*, *warning*, *danger*) indicherà la specifica struttura (rispettivamente: *situazione iniziale*, *movente*, *svolgimento della vicenda* e *conclusione*).


### Strutture a un solo livello

Inserendo un testo tra due coppie di segni `=` come in questo `==esempio==` si ottiene l'effetto ==evidenziato==.
Poiché l'effetto grafico è uno solo può essere usato semanticamente solo per situazioni in cui il valore è uno solo:

:::success
Fu, secondo che io già intesi, in ==Perugia== un giovane il cui nome era Andreuccio di Pietro, cozzone di cavalli
:::

Nell'esempio sopra tutto il testo in verde fa parte della **struttura narrativa** "situazione iniziale" e la città di Perugia è marcata come **luogo**, dimensione a cui non assegnamo ulteriori attributi.
Se invece volessimo distinguere i luoghi in *nazioni*, *città*, *quartieri*, oppure in *interni* ed *esterni* avremmo bisogno di un metodo più espressivo.


## Funzioni di Propp

Per questa dimensione di analisi si può usare il Blockquote Tags, sostituendo al nome dell'autore del commento il nome della funzione. Anche in questo caso il colore della barra laterale ha un valore semantico, coerente col nome della funzione.

> [name=ALLONTANAMENTO] [color=cyan] Testo da marcare

> [name=INFRAZIONE/DIVIETO][color=#FF0000] Testo da marcare

> [name=INVESTIGAZIONE] [color=yellow] Testo da marcare

> [name=DELAZIONE] [color=gray] Testo da marcare

> [name=TRANELLO] [color=blue] Testo da marcare

> [name=CONNIVENZA] [color=magenta] Testo da marcare

> [name=QUELLOCHEVOLETE] [color=#FF0305] Il testo che avete sottomano


Testo da marcare

Testo da marcare


## Sequenze

Può essere usata una marcatura con un unico effetto grafico, dove la semantica è espressa esplicitamente dal testo contenuto:

==sequenza narrativa==

opppure

`sequenza [narrativa | descrittiva | riflessiva]`


## Tempi

> [time=Domenica] [color=#907bf7]

> [time=la mattina dopo] 

> [time=a pranzo] [color=orange]

>[time=in tre ore] [color=yellow]


## Luoghi e Personaggi

I luoghi possono essere georeferenziati su OpenStreetMap; per i personaggi può essere usata la rubrica di Nextcloud per compilare schede strutturate. Le informazioni non gestite da campi specifici possono essere registrate nel campo note.


> Fu, secondo che io già intesi, in [Perugia](https://www.openstreetmap.org/#map=11/43.1398/12.2834) un giovane il cui nome era [Andreuccio di Pietro](https://cloud.narratron.io/apps/contacts/Tutti%20i%20contatti/ce435af3-0843-493a-af8f-14eb4aac8819~andreuccio-da-perugia), cozzone di cavalli; il quale, avendo inteso che a [Napoli](https://www.openstreetmap.org/#map=11/40.8537/14.2429) era buon mercato di cavalli, messisi in borsa cinquecento fiorin d’oro, non essendo mai più fuori di casa stato, con altri mercatanti là se n’andò:
> La [Pippo Franco](https://pad.narratron.io/s/HJkY2PTiI#) è l'altro personaggio ...

## Test marcatura con blockquote tags indentati

Per questa dimensione di analisi si può usare il Blockquote Tags, sostituendo al nome dell'autore del commento il nome della funzione. Anche in questo caso il colore della barra laterale ha un valore semantico, coerente col nome della funzione.

> [name=personaggio.Pippo] [color=cyan] Signor Pippo
>> [name=personaggio.Pippo.status] nobile
>> [name=personaggio.Pippo.scopi] rubare tutto
>> [name=personaggio.Pippo.amici] banda bassotti



> [name=INFRAZIONE/DIVIETO][color=#FF0000] Testo da marcare

> [name=INVESTIGAZIONE] [color=yellow] Testo da marcare
