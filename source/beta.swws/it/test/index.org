#+LANGUAGE: it
#+TITLE: Pagina di test
#+SUBTITLE: come vengono esportati alcuni elementi HTML5
#+DESCRIPTION: test delle features di esportazione di org-mode
#+KEYWORDS: SoftwareWorkers test
#+AUTHOR: Giovanni Biscuolo
#+EMAIL: giovanni.biscuolo@softwareworkers.it

#
# MACROS

# video(title,file,format[webm|mp4|ogg])
#+MACRO: video (eval (format "#+CAPTION: %1$s (video)\n#+ATTR_HTML: :controls controls :title %1$s :poster %4$s\n#+BEGIN_video\n#+HTML: <source src=\"%2$s\" type=\"video/%3$s\">\n[[.%2$s][%1$s (video)]]\n#+END_video" $1 $2 $3 $4))

#+NAME: abstract
#+BEGIN_ABSTRACT
In questa pagina facciamo i test di **alcuni** elementi introdotti in HTML5 e verifichiamo come vengono esportati.
#+END_ABSTRACT

* Elementi HTML5 utilizzabili come blocchi di testo

** article

#+ATTR_HTML: :itemscope "" :itemtype http://schema.org/BlogPosting
#+BEGIN_article
Questo è il sommario dell'articolo, nel quale viene riassunto in poche righe il contenuto in modo tale che il lettore possa decidere se il resto del contenuto è di suo interesse oppure no.
#+END_article

** aside

#+BEGIN_aside
Lorem ipsum dolor sit amet, consectetuer adipiscing elit.  Donec hendrerit tempor tellus.  Donec /pretium/ posuere tellus.

Aliquam **erat volutpat**.  Nunc eleifend leo vitae magna.  In id erat non orci commodo lobortis.  Proin neque massa, cursus ut, gravida ut, lobortis eget, lacus.  Sed diam.  Praesent fermentum tempor tellus.  Nullam tempus.

Etiam vel neque nec +dui dignissim bibendum+.  Vivamus id enim.  Phasellus neque orci, porta a, aliquet quis, semper a, massa.  Phasellus purus.  Pellentesque tristique imperdiet tortor.  Nam euismod tellus id erat.
#+END_aside

** audio

Questo è analogo all'elemento [[video]], ad eccezione del fatto che il cntenuto video dell'elemento non viene mostrato, solo quello audio.

** footer

Rappresenta il piè di pagina dell'elemento che lo ingloba (es. =article=), può quindi essere utilizzato **all'interno di un blocco** per aggiungere informazioni specifiche quali: documenti correlati, appendici, informazioni di dettaglio, ecc.; ad esempio per un blocco articolo:

#+ATTR_HTML: :itemscope "" :itemtype http://schema.org/BlogPosting
#+BEGIN_article
Questo è il sommario dell'articolo, nel quale viene riassunto in poche righe il contenuto in modo tale che il lettore possa decidere se il resto del contenuto è di suo interesse oppure no.
  #+BEGIN_footer
  Pubblicato il 20 Gennaio 1976 da Editori Riuniti.
  #+END_footer
#+END_article

** meter

Questo elemento rappresenta una misura scalare all'interno di un intervallo, o un valore frazionale; ad esempio l'occupazione disco o la frazione di votanti che hanno scelto una determinata opzione.  =meter= non deve essere utilizzato per indicare lo stato di avanzamento di un processo, per questo esiste l'elemento [[progress]].

Esempio:
biglietti venduti
#+ATTR_HTML: :min 0 :max 5000 :value 1450
#+BEGIN_meter
#+END_meter

** progress

Rappresenta lo stato di completamento di un compito; lo stato può essere indeterminato, indicando che c'è un avanzamento ma non è chiaro quando sia ancora necessario al completamento, oppure il completamento è in un intervallo da zero a un valore massimo configurabile.

Esempio:
Scaricamento file:
#+ATTR_HTML: :id file :max 2350 :value 1346
#+BEGIN_progress
#+END_progress

** summary/details

#+BEGIN_details
  #+BEGIN_summary
  Regione Lombardia
  #+END_summary
  La Regione Lombardia è una delle 21 regioni italiane, situata nella zona settentrionale al confine sud della Svizzera, confina a est con il Veneto e a Ovest con il Piemonte, bla bla bla....
#+END_details

** video

# FIXME: allow media files in the same directory as the page

#+NAME: video_esc2free
{{{video(Escape to freedom,/media/escape-to-freedom-360p.webm,webm)}}}

* Elementi HTML5 da non utilizzare

** canvas

Questo elemento fornisce una porzione di schermo nella quale è possibile disegnare elementi grafici attraverso l'uso di script in JavaScript.

Non usare questo elemento ma al suo posto usare un **blocco di codice org-babel** in uno dei linguaggi per disegnare diagrammi o grafici.

** figure (figcaption)

Per aggiungere note a illustrazioni, diagrammi, immagini, tabelle e simili utilizzare la keywork =CAPTION= subuto prima del blocco; ad es. per una immagine:

#+BEGIN_EXAMPLE
#+CAPTION: This is the caption for the next figure link (or table)
#+NAME:   fig:SED-HR4049
[[./img/a.jpg]]
#+END_EXAMPLE

** header

Non utilizzare l'elemento =header= perché viene già utilizzato dal backend di esportazione per incapsulare il titolo e il sottotitolo della pagina.

** menu

La struttura dei menù è tipicamente codificate nei template del nostro sistema di pubblicazione e per questo l'elemento =menu= è **riservato agli sviluppatori**.

** nav

La navigazione dell'intero sito o della singola pagina è tipicamente codificata nei template del nostro sistema di pubblicazione e per questo l'elemento =nav= è **riservato agli sviluppatori**.

** output

Questo elemento rappresenta il risultato di un calcolo, ad esempio quello eseguito da uno script (in JavaScript).

I risultati dell'esecuzione dei blocchi di codice (code blocks), come ad esempio calcoli eseguiti attraverso l'applicazione =calc=, sono gestiti direttamente da =org-mode=; per questo motivo =output= è un **elemento è deprecato** e al suo posto deve essere utilizzato un blocco di codice (Org Babel) opportunamente configurato.

** section

Evitare di usare esplicitamente questo elemento, dividere il documento in sezioni utilizzando la struttura ad heading di Org.
