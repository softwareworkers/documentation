---
title: PCTO Narratron - accesso DOCENTI
description: Istruzioni per l'accesso agli incontri online nell'ambito del PCTO Narratron
langs: it-it
robots: noindex, nofollow
tags: Narratron, PCTO, Liceo Novello, incontri

---

# BOZZA presentazione strumenti didattici liberi

Partiva dal Narratron al Novello, ma era destinata a catturare i docenti ai nuovi strumenti liberi.


# PCTO Narratron - accesso DOCENTI

## URL per l'accesso alla videoconferenza

Per semplificare l'accesso alle videoconferenze sul dominio narratron.io è stato attivato un dominio di terzo livello "**conf**", in modo da non essere vincolati ad una sola piattaforma, né a una sua specifica istanza.
sarà quindi sufficiente digitare ```conf.narratron.io``` sulla barra degli indirizzi (oppure cliccare sul link http://conf.narratron.io/) e il browser visiterà la URL https://meet.jit.si/narratron su cui *attualmente* si tengono le videoconferenze del PCTO Narratron.

Al primo accesso potrebbe essere chiesta l'installazione di componenti aggiuntivi. ++Non è necessario installare nulla++ 


![Schermata di accesso](https://pad.narratron.io/uploads/upload_d00c7d5d247bdb3cacf960ff5359fb0b.png)






## Orario di sabato 12 dicembre 2020

L'incontro si terrà dalle 8:00 alle 12:40, rispettando i cinque tempi scuola di 55', gli intervalli e le [linee guida DDI](https://drive.google.com/file/d/1wMsHSH29t7S5jZVDvCxnKchkew31UZDR/view) riguardanti il tempo di 40' per le attività sincrone e i 15' di attività asincrone.






## Uso della piattaforma di videoconferenza

### Jitsi

È la piattaforma utilizzata attualmente.
Non richiede la registrazione di un account; è richiesto di inserire il nome per usare la chat. È possibile proteggere le stanze con una password da condividere con tutti i partecipanti.

L'interfaccia è intuitiva, ma se servissero istruzioni suggeriamo di partire da questa pagina: http://scuola.linux.it/lezioni-online-jitsi (online ci sono molte altre guide disponibili, per esempio [qui](https://www.youtube.com/results?search_query=jitsi))



----


:::info
Da qui in poi il documento è in **bozza** in previsione di sviluppi futuri.
:::



### BigBlueButton

[BigBlueButton](https://bigbluebutton.org/) è una piattaforma per videoconferenze progettata per la didattica. È integrata con [Moodle](https://moodle.org/?lang=it), di prossima adozione all'interno del progetto Narratron.

